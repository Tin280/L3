import http.client
import requests
import json
target_hostname = "localhost"
target_port = 3000
default_timeout = 10
conn= http.client.HTTPConnection(
    host=target_hostname,
    port=target_port,
    timeout=default_timeout
)

headers= {"user-argent": "vscode-restclient","content-type": "application/json"}

conn.request("GET","/", headers=headers)
res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))
conn.request("GET","/data",headers=headers)
res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))
conn.request("GET", "/name-query?fname=Tin&lname=Nguyen", headers=headers)
res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))
conn.request("GET", "/rgb-to-hex?red=15&green=15&blue=15", headers=headers)
res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))
payload = "{\"num1\": 1.3,\"num2\": 2.4,\"client_name\": \"Tin\"}"
conn.request("POST","/add",payload, headers=headers)
response = conn.getresponse()
print(response.read().decode())
conn.request("GET","/hex-to-rgb?Hex=eb4034", headers=headers)
res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))